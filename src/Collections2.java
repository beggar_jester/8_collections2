public class Collections2 {
    public static void main(String[] args) {
        CustomHashSet<Integer> mySet = new CustomHashSet<>();
        System.out.println("Create CustomHashSet object and check its empty: "+mySet.isEmpty());
        System.out.println("Add some elements:");
        mySet.add(1);
        mySet.add(7);
        mySet.add(23);
        mySet.add(21);
        System.out.println(mySet);
        System.out.println("Try to add duplicate of element:");
        mySet.add(1);
        System.out.println(mySet);
        System.out.println("Check the set size:"+mySet.size());
        System.out.println("Check the set contains '17' element: "+mySet.contains(17));
        System.out.println("Check the set contains '23' element: "+mySet.contains(23));
        mySet.remove(23);
        System.out.println("Remove '23' from set:");
        System.out.println(mySet);
        hashSetIterator myIterator = mySet.iterator();
        System.out.println("Create iterator and display set elements data:");
        System.out.println(myIterator);
    }
}
