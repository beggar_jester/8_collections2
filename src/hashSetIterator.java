public class hashSetIterator {
    private Node currentNode;
    private int wentCounter =0;
    private int currentIndex =0;
    private int size;
    private final Node[] bucket;

    public hashSetIterator(int setSize, Node[] buckets ){
        bucket = buckets.clone();
        currentNode = bucket[currentIndex];
        size = setSize;
        getNextNode();
    }

    public boolean hasNext(){
        return wentCounter < size;
    }

    public Node next(){
        Node result = currentNode;
        wentCounter++;
        getNextNode();
        return result;
    }

    private void getNextNode(){
        if(wentCounter < size){
            if(currentNode != null && currentNode.getNext() != null){
                currentNode = currentNode.getNext();
            }
            else {
                currentIndex++;
                currentNode = bucket[currentIndex];
                if(currentNode != null ) return;
                getNextNode();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[ ");
        while (hasNext()) result.append(next().getData()).append(" ");
        result.append("]");
        return result.toString();
    }
}
