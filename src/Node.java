public class Node<typeName> {
    private typeName Data;
    Node Next;

    public Node(typeName data) {
        Data = data;
    }

    public Node getNext() {
        return Next;
    }

    public typeName getData() {
        return Data;
    }

    @Override
    public String toString() {
        return "Node{" +
                "Data=" + Data +
                '}';
    }
}
