public class CustomHashSet<typeName> {

    private static final int Capacity = 10;
    private int hashSetSize = 0;
    private Node[] buckets;
    private final double loadFactor = 0.75;

    public CustomHashSet() {
        buckets = new Node[Capacity];
    }

    public void add(typeName elementToAdd) {
        int key = getHashCode(elementToAdd);
        Node currentBucket = buckets[key];
        while (currentBucket != null) {
            if (currentBucket.getData().equals(elementToAdd)) return;
            currentBucket = currentBucket.getNext();
        }
        if (buckets[key] == null) {
            buckets[key] = new Node<>(elementToAdd);
        } else {
            Node<typeName> newNode = new Node<>(elementToAdd);
            newNode.Next = buckets[key];
            buckets[key] = newNode;
        }
        hashSetSize++;
        if (hashSetSize > buckets.length * loadFactor) reHashSet();
    }

    public boolean contains(typeName elementToFind) {
        int key = getHashCode(elementToFind);
        Node currentBucket = buckets[key];
        while (currentBucket != null) {
            if (currentBucket.getData().equals(elementToFind)) return true;
            currentBucket = currentBucket.getNext();
        }
        return false;
    }

    public boolean isEmpty() {
        return hashSetSize == 0;
    }

    public int size() {
        return hashSetSize;
    }

    public typeName remove(typeName elementToRemove) {
        if (!contains(elementToRemove))
            throw new IllegalArgumentException(elementToRemove + " element does not exists.");
        hashSetSize--;
        int key = getHashCode(elementToRemove);
        Node currentBucket = buckets[key];
        if (currentBucket.getData() == elementToRemove) buckets[key] = currentBucket.getNext();
        while (currentBucket.getNext() != null) {
            if (currentBucket.getNext().getData().equals(elementToRemove)) {
                currentBucket.Next = currentBucket.getNext().getNext();
                return (typeName) currentBucket.getData();
            }
            if (currentBucket.getNext() != null) currentBucket = currentBucket.getNext();
        }
        return elementToRemove;
    }

    public hashSetIterator iterator(){
        return new hashSetIterator(hashSetSize, buckets);
    }

    private int getHashCode(typeName element) {
        return Math.abs(element.hashCode()) % buckets.length;
    }

    private void reHashSet() {
        Node[] temp = buckets.clone();
        buckets = new Node[buckets.length * 2];
        hashSetSize = 0;
        for (Node branch : temp) {
            while (branch != null) {
                add((typeName) branch.getData());
                branch = branch.getNext();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("CustomHashSet{  ");

        for (Node element : buckets) {
            while (element != null) {
                result.append(element).append(" ");

                element = element.getNext();
            }
        }
        result.append(" }");
        return result.toString();
    }
}
